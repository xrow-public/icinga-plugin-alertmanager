# icinga-plugin-alertmanager

The icinga plugin is used to integrate alertmanager / kubernetes into icinga. The inital setup might be a challange, but the later use in iginca is trivial. Unter the hood the plugin is talking to the alertmanager api with the [amtool](https://github.com/prometheus/alertmanager?tab=readme-ov-file#amtool)

# Install

* Apply the ansible role to the server
* Insert icinga director basket into icinga
* Setup a alertmanager in your kubernetes 
* Apply the alertmanager setting ForceEnableClusterMode: true
* Setup a alertmanager host in icinga with a access token for public routes or without for private routes.

