#!/bin/bash

helm repo add icinga https://icinga.github.io/helm-charts

helm upgrade --install icinga --create-namespace -n icinga icinga/icinga-stack -f values.yaml
