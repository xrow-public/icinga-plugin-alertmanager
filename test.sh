#!/bin/bash

source ./.env.local

./check_altertmanager -a TargetDown
echo "----------------"; echo ""

./check_altertmanager -q 'namespace="redmine"' -a CPUThrottlingHigh
echo "----------------"; echo ""

./check_altertmanager -a Watchdog
echo "----------------"; echo ""

./check_altertmanager -q 'namespace="icinga"' -a KubePodCrashLooping
echo "----------------"; echo ""

./check_altertmanager -q 'namespace=~".*"' -a KubePodCrashLooping
echo "----------------"; echo ""

./check_altertmanager -q 'namespace="cattle-monitoring-system"' -a AlertmanagerClusterDown
echo "----------------"; echo ""
./check_altertmanager -q 'namespace="cattle-monitoring-system"' -a etcdDatabaseHighFragmentationRatio
